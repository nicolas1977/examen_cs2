package examePack;
import java.text.DecimalFormat;
import examePack.MultiStore;
import javafx.util.Pair;



    /* Dopo un esame arrivano al programma i dati relativi le domande con le risposte corrette.
Per ogni alunno vengono passati i seguenti dati.
Un numero N che identifica il numero di domande del quiz.
Una stringa di lunghezza N che identifica la risposta corretta a ogni domanda.
Ogni domanda ha 5 risposte di cui solo una corretta, le risposte sono identificate con le lettere A B C D E.
Un array di stringhe.
Ogni stringa fa riferimento a una domanda con la risposta data dall'alunno.
Se una risposta e' selezionata nella stringa e' identificata con il carattere X, altrimenti con 0.
Ogni domanda puo' avere solo 1 risposta valida.
Il punteggio massimo del quiz e' 100.
Calcola il punteggio finale.


Marco Carettoni14:40
int n = 4
string risposte = DBCA
list =
000X0
0X000
0X000
X0000

RISPOSTA: 75%
ORIGINAL:
assignetor(6, "CADAED", new String[]{"00X00",
                    "X0000",
                    "000X0",
                    "X0000",
                    "0000X",
                    "000X0"})

Author : Nicolas D'Aversa
*/

public class Exame {
    public static void main(String args[]) {
        try{
            /*calcolaPunteggio*/
            double result = percentator(comparator(assignetor(6, "CADAED", new String[]{"00X00",
                    "X0000",
                    "000X0",
                    "X0000",
                    "0000X",
                    "000X0"})));
            DecimalFormat decimalFormat = new DecimalFormat("##.###");
            System.out.println("il resultato delle risposte in percentuale è il seguente: " + decimalFormat.format(result) + "%");

            System.out.println("program finished");
        }catch(Exception e){
            System.out.println("error incur:" + e.getMessage());
        }
    }



    //function percentator
    public static double percentator (MultiStore multi){
        double percento=0;
        try{
            Double nd1 = (double) multi.nExacte;
            Double nd2= (double) multi.nDemande;
            //or : percento = (Double.valueOf(multi.nExacte)) / (Double.valueOf(multi.nDemande)) * 100.0;
            percento = (nd1 / nd2) * 100.0;

        }catch(Exception e){
            System.out.println("error incur1:" + e.getMessage());
        }finally {
               System.out.println("ok executing parcentator");
        }
        return percento;
    }

    //effettuo la comparazione
    //individuare l'k risposte esatte
    //
    //function comparator
    public static MultiStore comparator(MultiStore multi){
        int k=0;
        try{
            int i=0;
            int j=0;
            do {
                j=0;
                do{
                    if (multi.ass[i][0][0]==multi.ass[j][2][0] && multi.ass[i][1][j]=='X'){
                        k=k+1;
                        break;
                    }
                    j=j+1;
                 }while(j<multi.ass[0][1].length);
                /*This is for a 3 dimensional array.
                int x[][][]=new int[5][8][10];
                System.out.println(x.length+" "+x[1].length+" "+x[0][1].length);
                OUTPUT : 5 8 10
                https://stackoverflow.com/questions/5958186/multidimensional-arrays-lengths-in-java#:~:text=In%20java%20we%20can%20define,%5B%5D%20be%20my%20array%20a.*/
                i = i + 1;
            }while(!(multi.ass[i][0][0]==0));
            /*if (position[i][j] == '\u0000')
            or use this if you want to improve readability:
            if (positionv[i][j] == 0)*/
        }catch(Exception e){
            System.out.println("error incur2:" + e.getMessage());
        }finally {
            System.out.println("ok executing comparator");
        }
        multi.nExacte=k;
        return multi;
    }


    // function assignetor
    public static MultiStore assignetor(int n, String risposte, String ar[]) {
        char ass[][][];
        ass = new char[50][3][ar.length-1];
        Pair<char[][][], Integer> pair;
        try {
            String alpha = "ABCDEFGHLMNOPQRSTUVZ";
            int i = -1;
            int j = 0;
            do {
                i = i + 1;
                //le risposte CADEAD esatte
                ass[i][0][0] = (risposte.charAt(i));
                j = -1;
                do {
                    j = j + 1;
                    //l' i-eme stringa 0X000 de la risposta alunno
                    ass[i][1][j] = ar[i].charAt(j);

                } while (j < ar.length - 2);
                //elementi dell'alfabeto ABCDEFGHLMNOPQRSTUVZ
                ass[i][2][0] = alpha.charAt(i);
            } while (i < ar.length-1);

        } catch (Exception e) {
            System.out.println("error incur3:" + e.getMessage());
        } finally {
            System.out.println("ok executing assignetor");
        }
        MultiStore multi = new MultiStore(ass, n);
        return multi;
    }
}

